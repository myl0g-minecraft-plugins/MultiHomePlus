package com.milogilad.multihomeplus;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public final class MultiHomePlus extends JavaPlugin {

	@Override
	public void onEnable() {
		super.onEnable();
		ConfigurationSerialization.registerClass(Home.class);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			sender.sendMessage("The console can't use this command!");
			return false;
		}

		if (cmd.getName().equalsIgnoreCase("sethome")) {
			if (!sender.hasPermission("multihomeplus.sethome")) {
				sender.sendMessage("You're not permitted to use that command!");
				return false;
			}

			if (args.length < 1) {
				sender.sendMessage("§cNot enough arguments!");
				return false;
			}

			setHome(args[0], (Player) sender);
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("home")) {
			if (!sender.hasPermission("multihomeplus.home")) {
				sender.sendMessage("You're not permitted to use that command!");
				return false;
			}

			if (args.length < 1) {
				sender.sendMessage("§cNot enough arguments!");
				return false;
			}

			Location warp = getHome(args[0], (Player) sender);

			if (warp == null) {
				sender.sendMessage("That home does not exist!");
				return false;
			}

			timeOutTeleport(((Player) sender), warp, 5);

			return true;
		}

		if (cmd.getName().equalsIgnoreCase("listhomes")) {
			if (!sender.hasPermission("multihomeplus.listhomes")) {
				sender.sendMessage("You're not permitted to use that command!");
				return false;
			}

			sender.sendMessage(getHomesAsString((Player) sender));
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("delhome")) {
			if (!sender.hasPermission("multihomeplus.delhome")) {
				sender.sendMessage("You're not permitted to use that command!");
				return false;
			}

			if (args.length < 1) {
				sender.sendMessage("§cNot enough arguments!");
				return false;
			}

			deleteHome(args[0], (Player) sender);
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("importhome")) {
			if (!sender.hasPermission("multihomeplus.importhome")) {
				sender.sendMessage("You're not permitted to use that command!");
				return false;
			}

			if (args.length < 1) {
				sender.sendMessage("§cNot enough arguments!");
				return false;
			}

			importHome((Player) sender, args[0]);
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("migratehome")) {
			if (!sender.hasPermission("multihomeplus.migratehome")) {
				sender.sendMessage("You're not permitted to use that command!");
				return false;
			}

			if (args.length < 1) {
				sender.sendMessage("§cNot enough arguments!");
				return false;
			}

			migrateHome((Player) sender, args[0]);
			return true;
		}

		return false;
	}

	private void timeOutTeleport(Player p, Location l, int timeInSeconds) {
		p.sendMessage("§9Teleporting in " + Integer.toString(timeInSeconds) + " seconds, don't move!");
		String init = p.getLocation().toString();

		BukkitRunnable task = new BukkitRunnable() {
			@Override
			public void run() {
				if (init.equals(p.getLocation().toString())) {
					p.teleport(l);
					p.sendMessage("§aTeleportation successful.");
				} else {
					p.sendMessage("§cYou moved before you could be teleported!");
				}
			}
		};

		task.runTaskLater(this, 20 * timeInSeconds);
	}

	private List<Home> getRegisteredHomes(Player p) {
		return getConfig().getList(p.getUniqueId().toString(), Arrays.asList()).stream().map(object -> (Home) object)
				.collect(Collectors.toList());
	}

	private void setRegisteredHomes(Player p, List<Home> homes) {
		getConfig().set(p.getUniqueId().toString(), homes);
		saveConfig();
	}

	private Location getHome(String name, Player p) {
		for (Home h : getRegisteredHomes(p)) {
			if (h.getName().equals(name)) {
				return h.getLocation();
			}
		}
		return null;
	}

	private void setHome(String name, Player p) {
		setHome(name, p, p.getLocation());
	}

	private boolean canSetHome(String newHomeName, Player p) {
		List<Home> registeredHomes = getRegisteredHomes(p);

		boolean homeAlreadyExists = false;

		for (Home h : registeredHomes) {
			if (h.getName().equals(newHomeName)) {
				homeAlreadyExists = true;
				break;
			}
		}

		return homeAlreadyExists || (registeredHomes.size() < 2);
	}

	private void setHome(String name, Player p, Location l) {
		Home h = null;
		List<Home> registeredHomes = getRegisteredHomes(p);

		for (Home testHome : registeredHomes) {
			if (testHome.getName().equals(name)) {
				h = testHome;
				registeredHomes.remove(testHome);
				break;
			}
		}

		if (h == null) {
			if (!canSetHome(name, p)) {
				p.sendMessage("§cCan't create this home; you've reached your max allowed!");
				return;
			}
			p.sendMessage("§9'" + name + "' doesn't exist. Making it now...");
			h = new Home(name, l);
		} else {
			p.sendMessage("§9Updating '" + name + "' with your current location...");
			registeredHomes.remove(h);
			h.setLocation(l);
		}

		registeredHomes.add(h);
		setRegisteredHomes(p, registeredHomes);
		p.sendMessage("§a'" + name + "' set!");
	}

	private String getHomesAsString(Player p) {
		String result = "";
		for (Home h : getRegisteredHomes(p)) {
			result += h.getName() + " (X = " + Double.toString(h.getLocation().getX()) + ", Y = "
					+ Double.toString(h.getLocation().getY()) + ", Z = " + Double.toString(h.getLocation().getZ())
					+ "); ";
		}

		return result;
	}

	private void deleteHome(String name, Player p) {
		List<Home> registeredHomes = getRegisteredHomes(p);
		Home desired = null;

		for (Home h : registeredHomes) {
			if (h.getName().equals(name)) {
				desired = h;
				break;
			}
		}

		if (desired == null) {
			p.sendMessage("§cThat home does not exist!");
			return;
		}

		registeredHomes.remove(desired);
		p.sendMessage("§aRemoved your home '" + name + "'.");
		setRegisteredHomes(p, registeredHomes);
	}

	private void importHome(Player p, String name) {
		FileConfiguration old = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "old.yml"));
		String mainPath = p.getUniqueId().toString() + "." + name;

		Location l = new Location(Bukkit.getWorld(old.getString(mainPath + ".world")), old.getDouble(mainPath + ".x"),
				old.getDouble(mainPath + ".y"), old.getDouble(mainPath + ".z"));

		setHome(name, p, l);
	}

	private void migrateHome(Player p, String name) {
		FileConfiguration old = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "v1.yml"));
		setHome(name, p, (Location) old.get(p.getUniqueId().toString() + "." + name));
	}
}
