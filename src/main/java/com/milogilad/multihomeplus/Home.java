package com.milogilad.multihomeplus;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

public class Home implements ConfigurationSerializable {
    private Location location;
    private String name;

    public static Home deserialize(Map<String, Object> m) {
        return new Home((String) m.get("name"), (Location) m.get("location"));
    }

    public static Home valueOf(Map<String, Object> m) {
        return deserialize(m);
    }

    public Home(String name, Location l) {
        this.name = name;
        this.location = l;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location l) {
        this.location = l;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<String, Object>();

        data.put("name", this.name);
        data.put("location", this.location);

        return data;
    }
}
